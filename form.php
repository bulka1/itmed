<?php

class mainClass
{
    public $string;
    public $errors = [];

    public function __construct($string)
    {
        $this->string = $string;
    }

    public function validate()
    {
        $stack = array();
        $token = strtok($this->string, "");
        $token = explode(" ", $token);

        $endstr = false;
        while ($endstr != true) {

            if ($token[0] == 0 and preg_match("/[\+\-\*\/\^]/", $token[0])) {
                $f = $token[0];
                unset($token[0]);
                array_push($token, $f);
            }

            foreach ($token as $key => $value) {


                if (preg_match("/^[0-9]*[\+\-\*\/\^]?[0-9]/", $value)) {


                    array_push($stack, $value);

                } else if (preg_match("/[\+\-\*\/\^]/", $value)) {

                    if (is_numeric($value)) {
                        array_push($stack, $value);

                    }

                    if (count($stack) < 2) {

                        $this->errors = ["Недостаточно данных в стеке для операции '$value'"];

                        return false;
                    }


                    $b = array_shift($stack);

                    $a = array_shift($stack);

                    switch ($value) {
                        case '*':
                            $res = $a * $b;
                            break;
                        case '/':
                            $res = $a / $b;
                            break;
                        case '+':
                            $res = $a + $b;
                            break;
                        case '-':
                            $res = $a - $b;
                            break;
                        case '+-':
                            $res = $a + -$b;
                            break;
                    }
                    array_push($stack, $res);

                } else {

                    $this->errors = ["Недопустимый символ в выражении: $value"];
                    return false;
                }
            }
            $endstr = true;
        }

        if (count($stack) > 1) {

            $this->errors = ["Количество операторов не соответствует количеству операндов"];
            return array_pop($stack);
        }
        return array_pop($stack);
    }

}

if (isset($_POST['expression'])) {
    $expression = new mainClass($_POST['expression']);
    $expression->validate();
    include 'index.php';
}




