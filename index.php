<!DOCTYPE html>
<html>
<head>
  <title>Test</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

  <!-- JQUERY -->
  <link rel="stylesheet" href="main.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Test</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/index.php">#1</a></li>
      </ul>
      <ul class="nav navbar-nav">
        <li><a href="/test2.php">#2</a></li>
      </ul>

    </div>
  </div>
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <form method="post" action="/form.php">

        <div class="form-group">
          <label>Enter your expression</label>
          <input type="text" class="form-control" placeholder="Enter" name="expression">
            <?php if ($expression->errors): ?>
          <?php foreach ($expression->errors as $error): ?>
                <span>Строка: <?php  echo $expression->string ?></span>
              <span><?php echo $error ?></span>
          <?php endforeach; ?>
          <?php elseif($expression->string): ?>
              <span>Строка: <?php  echo $expression->string ?></span>
              <span>Результат: <?php  echo $expression->validate() ?></span>
          <?php endif; ?>
        </div>

        <div class="form-group">
          <input type="submit" class="btn btn-success">
        </div>

      </form>
    </div>
  </div>
</div>

</body>

</html>